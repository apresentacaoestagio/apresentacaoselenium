package com.company;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Main {
    public static void main(String[] args) throws InterruptedException {

        ////////////////////////////////////////////////
        // Alexandre Varela & Alexandre Hilário 2020  //
        ////////////////////////////////////////////////

        // variável resultado
        boolean testePassou;

        // Propriedades e inicialização chromeDriver 87
        System.setProperty("webdriver.chrome.driver","libs/chromedriver.exe");      // Caminho para o webDriver
        WebDriver webdriver = new ChromeDriver();                                   // Tipo de Webdriver
        webdriver.get("http://zero.webappsecurity.com/");                           // URL do website
        webdriver.manage().window().maximize();                                     // Maximiza a janela

        // Espera que o elemento carregue e clica no botão de login
        waitForElement(webdriver, "//*[@id=\"signin_button\"]", 5);         // Espera pelo elemento
        webdriver.findElement(By.xpath("//*[@id=\"signin_button\"]")).click();               // Carrega no botão

        // Faz login
        webdriver.findElement(By.xpath("//*[@id=\"user_login\"]")).sendKeys("username");    // coloca o username no input
        WebElement password = webdriver.findElement(By.xpath("//*[@id=\"user_password\"]"));           // Armazena o objeto da password
        password.sendKeys("password");                                                      // coloca a password no input
        password.submit();                                                                             // faz o submit

        // Verifica se o username é igual
        waitForElement(webdriver, "/html/body/div[1]/div[1]/div/div/div/ul/li[3]/a", 5);                               // espera pelo elemento
        testePassou = webdriver.findElement(By.xpath("/html/body/div[1]/div[1]/div/div/div/ul/li[3]/a")).getText().equals("username");  //recebe o resultado da verificação

        // Mostra o resultado final do teste
        if(testePassou) System.out.println("Resultado Teste: Passou");
        else System.out.println("Resultado Teste: Falhou");

        // Espera 1 segundo e fecha o browser
        Thread.sleep(1000);
        webdriver.close();
    }

    /**
     * Espera que o elemento carregue
     * @param driverTemp webdriver
     * @param xPathTemp xpath objeto
     * @param timeout tempo de timeout
     */
    public static void waitForElement(WebDriver driverTemp, String xPathTemp, int timeout) {
        try {
            // Espera que o elemento carregue
            WebDriverWait wait = new WebDriverWait(driverTemp, timeout);
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPathTemp)));
        }catch(TimeoutException e) {
            System.out.println("Element: " + xPathTemp + " not found");
        }catch(Exception e) {
            System.out.println("Generic Exception: " + e.getMessage());
        }
    }
}
